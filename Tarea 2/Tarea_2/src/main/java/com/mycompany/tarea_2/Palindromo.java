/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tarea_2;

import java.util.Scanner;

/**
 *
 * @author Jesús21
 */
public class Palindromo {
    public static void main(String[] args) {
        
    Scanner x = new Scanner(System.in);
    String palabra;
    char [] palindromo;
    int izq, der;
    System.out.println("Ingrese la palabra");
    palabra = x.nextLine();
    palabra = palabra.toLowerCase();//para que no importe la mayuscula
    palabra = palabra.replace(" ", "");
    palindromo = palabra.toCharArray();
    izq=0;
    der=palindromo.length-1;
    
    while(izq <der){
        if(palindromo[izq]==palindromo[der]){
            der --;
            izq++;
        }else{
            System.out.println("La palabra no es palindromo");
            break;
        }
    }
    if(izq == der){
        System.out.println("La palabra es palindromo");
    }
        
    }
    
}
