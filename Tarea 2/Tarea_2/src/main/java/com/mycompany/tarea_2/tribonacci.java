/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tarea_2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Jesús21
 */
public class tribonacci {
    public static void main(String [] args) throws IOException{
    
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Ingrese el Tamaño: ");
    int tamano = Integer.parseInt(br.readLine());
    int num1=0;
    int num2=0;
    int num3=1;
        
 
    //Creamos un vector que contenga la sucesión de números
    int [] elementos = new int[tamano];
    elementos[0] = num1;
    elementos[1] = num2;
    elementos[2] = num3;

//Cargamos el vector con la sucesión de números
for (int i = 3; i < elementos.length; i++) {
    elementos[i] = elementos[i-1] + elementos[i-2]+ elementos[i-3];
}
    
//creamos un indice/contador para recorrer el vector
int contador = -1;
int [] arreglo = new int [tamano];
for (int i = 0; i < arreglo.length; i++) {
    
    contador++;
    arreglo[i] = elementos[contador];
}
for (int i = 0; i < arreglo.length; i++) {
    System.out.print(arreglo[i] + "\t");
}
System.out.println();
}
      
}
